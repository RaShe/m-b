// CLI
const meteorController = require('./controller/meteors.controller');
const readline = require('readline');

let year = null;
let mass = null;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Please specify year or press enter to continue ', yearInput => {
    year = yearInput;

    rl.question('Please specify mass or press enter to continue ', async massInput => {
        mass = massInput;
        console.log('Working');
        const outputData = await meteorController.meteorLanding(year, mass);
        console.log(outputData);
        rl.close();
    });
});
