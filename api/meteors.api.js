const meteorController = require('../controller/meteors.controller');

exports.meteorLanding = async ctx => {
    const { year, mass } = ctx.request.query;
    ctx.body = await meteorController.meteorLanding(year, mass);
};
