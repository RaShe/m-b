const Koa = require('koa');
const router = require('koa-router')();
const convert = require('koa-convert');
const koaRes = require('koa-res');

const meteorController = require('./api/meteors.api');

const app = new Koa();

// error handling
app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        ctx.status = err.status || 500;
        ctx.body = err.message;
        ctx.app.emit('error', err, ctx);
    }
});

app.use(convert(koaRes()));

router.get('/echo', ctx => ctx.body = 'pew pew');

// Example - http://localhost:3075/meteor-landing?year=1984&mass=320
router.get('/meteor-landing', meteorController.meteorLanding);

app.use(router.routes());

const server = app.listen(3075, () => {
    console.log('app running');
});

module.exports = server;
