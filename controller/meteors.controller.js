// TODO: In real app we should use Kube credentials
const _meteorsApi = 'https://data.nasa.gov/resource/y77d-th95.json';
const _marsWeatherApi = 'https://api.nasa.gov/insight_weather/?api_key=itMoPjsz28QTLS4EDD0027T0N6zuQWDx4cLF39YP&feedtype=json&ver=1.0';
const rp = require('request-promise');

exports.meteorLanding = async (year, mass) => {
    const massNumber = mass ? Number(mass) : null;
    const yearNumber = year ? Number(year) : null;

    const results = {
        isError: false,
        errorMessage: '',
        content: []
    };

    const [meteorsDataList, marsWeather] = await Promise.all([
        getMeteorsData(),
        getMarsWeatherData()
    ]);

    const marsHWS = handleWeatherData(marsWeather);

    if (!Array.isArray(meteorsDataList)) {
        results.isError = true;
        results.errorMessage = 'Nasa api did not return any data';
    } else if (!year && !mass) {
        results.content = meteorsDataList;
    } else {
        // we use FOR instead of MAP or FILTER to iterate results faster
        for (let i = 0; i < meteorsDataList.length; i++) {
            const meteor = meteorsDataList[i];

            let valid = true;

            if (massNumber) {
                const meteorMass = meteor.mass ? Number(meteor.mass) : null;
                if (meteorMass !== massNumber) {
                    valid = false;
                }
            }

            if (valid && yearNumber) {
                const meteorYear = meteor.year;
                if (meteorYear) {
                    const relevantYearPart = meteorYear.split('-');
                    if (relevantYearPart.length > 0) {
                        const meteorYearNumber = Number(relevantYearPart[0]);
                        if (meteorYearNumber !== yearNumber) {
                            valid = false;
                        }
                    } else {
                        valid = false;
                    }
                } else {
                    valid = false;
                }
            }

            if (valid) {
                meteor.HWS = marsHWS;
                results.content.push(meteor);
            }
        }
    }

    return results;
};

const handleWeatherData = weatherData => {
    let result = null;

    if (weatherData && Array.isArray(weatherData.sol_keys) && weatherData.sol_keys.length > 0) {
        const solKeys = weatherData.sol_keys;
        const lastSolKey = solKeys[solKeys.length - 1];
        const lastSol = weatherData[lastSolKey];
        if (lastSol) {
            result = lastSol.HWS;
        }
    }

    return result;
};

const getMeteorsData = () => {
    const requestOptions = {
        json: true,
        uri: _meteorsApi
    };

    return callToApi(requestOptions);
};

const getMarsWeatherData = () => {
    const requestOptions = {
        json: true,
        uri: _marsWeatherApi
    };
    return callToApi(requestOptions);
};

const callToApi = async options => {
    let result = null;
    try {
        const data = await rp(options);
        if (data) {
            result = data;
        }
    } catch (e) {
        console.error(e);
    }
    return result;
};
