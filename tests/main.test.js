const request = require('supertest');

const server = require('../app.js');

const meteorMock = require('./mocks/meteorData.mock.json');

beforeAll(() => {
    console.log('Test started');
});

afterAll(() => {
    server.close();
    console.log('Test end');
});

describe('meteor-landing test', () => {
    test('test year', async () => {
        const response = await request(server).get('/meteor-landing?year=1984').send(meteorMock);
        expect(response.status).toEqual(200);
        expect(response.body.data.isError).toBeFalsy();
        expect(response.body.data.content.length).toEqual(7);
    });

    test('test year and mass', async () => {
        const response = await request(server).get('/meteor-landing?year=1984&mass=320').send(meteorMock);
        expect(response.status).toEqual(200);
        expect(response.body.data.isError).toBeFalsy();
        expect(response.body.data.content.length).toEqual(1);
    });
});
